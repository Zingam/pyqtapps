#!/usr/bin/env python
__author__ = 'Hristo'

import os
import fnmatch
import sys
import platform
import subprocess

print("Building PyQt forms from QtDesigner .ui files")
print("WARNING: Does not work if filename contains whitespace characters!")

# Check for old files and delete them
pathForms = "src/forms/"

for file in os.listdir(pathForms):
    if fnmatch.fnmatch(file, "ui_*"):
        print("Deleting old file: " + file)
        os.remove(pathForms + file)

# if os.path.isfile(file):
#     print("Deleting old files: " + file)
#     os.remove(file)

# Find PyQt5 system path
pathPython = sys.path
pathPyQt5 = ""

for path in pathPython:
    if path.find("site-packages"):
        pathPyQt5 = path

pathPyQt5 += "/PyQt5"
if platform.system() == "Windows":
    pathPyQt5 = pathPyQt5.replace("/", "\\")

print("PyQt5 location: " + pathPyQt5)

# Find this script's path
scriptPath = os.path.dirname(os.path.realpath(__file__)) + "/"
pathFormsFull = scriptPath + pathForms

# Find .ui files and build them
for file in os.listdir(pathForms):
    if fnmatch.fnmatch(file, "*.ui"):
        # Build and execute the command
        # /pyuic5 builds .py files from QtDesigner .ui files
        command = pathPyQt5 + "/pyuic5" + " "
        command += pathFormsFull + file + " "
        newFileName = file.rstrip(".ui")
        command += "-o" + " "
        command += pathFormsFull + "ui_" + newFileName + ".py"

        if platform.system() == "Windows":
            command = command.replace("/", "\\")

        print("Executing: " + command)

        # Call a system process
        try:
            subprocess.call(command, shell=True)
        except subprocess.CalledProcessError as error:
            print("Failed to execute command")
            print(command)
            print("Error: " + error.output)


