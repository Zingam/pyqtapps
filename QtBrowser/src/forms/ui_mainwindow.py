# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Hristo\Documents\GitHub\PyQtApps\QtBrowser\src\forms\mainwindow.ui'
#
# Created: Tue Jul 22 14:53:51 2014
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(704, 530)
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setObjectName("centralWidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralWidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.pushButton_Back = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_Back.setObjectName("pushButton_Back")
        self.horizontalLayout.addWidget(self.pushButton_Back)
        self.pushButton_Forward = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_Forward.setObjectName("pushButton_Forward")
        self.horizontalLayout.addWidget(self.pushButton_Forward)
        self.lineEdit_AddressBar = QtWidgets.QLineEdit(self.centralWidget)
        self.lineEdit_AddressBar.setObjectName("lineEdit_AddressBar")
        self.horizontalLayout.addWidget(self.lineEdit_AddressBar)
        self.pushButton_Go = QtWidgets.QPushButton(self.centralWidget)
        self.pushButton_Go.setObjectName("pushButton_Go")
        self.horizontalLayout.addWidget(self.pushButton_Go)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.webView = QtWebKitWidgets.QWebView(self.centralWidget)
        self.webView.setUrl(QtCore.QUrl("about:blank"))
        self.webView.setObjectName("webView")
        self.verticalLayout.addWidget(self.webView)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralWidget)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "QtBrowser"))
        self.pushButton_Back.setText(_translate("MainWindow", "<"))
        self.pushButton_Forward.setText(_translate("MainWindow", ">"))
        self.pushButton_Go.setText(_translate("MainWindow", "Go"))

from PyQt5 import QtWebKitWidgets
