__author__ = 'Hristo'

from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtWebKit import QWebSettings
from PyQt5.QtCore import QUrl

# import MainWindow
from forms.ui_mainwindow import Ui_MainWindow


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, url):
        QMainWindow.__init__(self)
        self.setupUi(self)

        # Enable plugins
        QWebSettings.globalSettings().setAttribute(QWebSettings.PluginsEnabled, True)

        # UI: connect signals to slots
        self.pushButton_Back.clicked.connect(self.on_pushButton_Back_clicked)
        self.pushButton_Forward.clicked.connect(self.on_pushButton_Forward_clicked)
        self.pushButton_Go.clicked.connect(self.on_pushButton_Go_clicked)
        self.lineEdit_AddressBar.returnPressed.connect(self.on_pushButton_Go_clicked)

        self.webView.titleChanged.connect(self.onTitleChanged)
        self.webView.loadFinished.connect(self.onLoadingNewPage)

        self.webView.load(url)

# UI SLOTS
    def on_pushButton_Back_clicked(self):
        webHistory = self.webView.history()
        webHistory.back()


    def on_pushButton_Forward_clicked(self):
        webHistory = self.webView.history()
        webHistory.forward()

    def on_pushButton_Go_clicked(self):
        url = QUrl.fromUserInput(self.lineEdit_AddressBar.text())
        self.webView.load(url)

# APP SLOTS
    def onTitleChanged(self):
        title = self.webView.title()
        self.setWindowTitle(title)

    def onLoadingNewPage(self):
        url = self.webView.url().toString()
        self.lineEdit_AddressBar.setText(url)
