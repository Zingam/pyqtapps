__author__ = 'Hristo'

import sys
from PyQt5.QtWidgets import (QApplication)
from PyQt5.QtCore import QUrl

from forms.mainwindow import MainWindow


if __name__ == "__main__":
    a = QApplication(sys.argv)

    # Load default URL
    url = QUrl("http://www.google.com")

    w = MainWindow(url)
    w.show()

    a.exec_()