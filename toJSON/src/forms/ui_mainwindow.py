# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Hristo\Documents\GitHub\PyQtApps\toJSON\src\forms\mainwindow.ui'
#
# Created: Tue Jul 22 14:54:49 2014
#      by: PyQt5 UI code generator 5.3.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        MainWindow.setMinimumSize(QtCore.QSize(800, 600))
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label_Title = QtWidgets.QLabel(self.centralwidget)
        self.label_Title.setMaximumSize(QtCore.QSize(16777215, 50))
        self.label_Title.setObjectName("label_Title")
        self.verticalLayout.addWidget(self.label_Title)
        self.groupBox_FileSelect = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_FileSelect.setMaximumSize(QtCore.QSize(16777215, 80))
        self.groupBox_FileSelect.setObjectName("groupBox_FileSelect")
        self.verticalLayout_4 = QtWidgets.QVBoxLayout(self.groupBox_FileSelect)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.horizontalLayout_FileSelect = QtWidgets.QHBoxLayout()
        self.horizontalLayout_FileSelect.setObjectName("horizontalLayout_FileSelect")
        self.lineEdit_FileSelect = QtWidgets.QLineEdit(self.groupBox_FileSelect)
        self.lineEdit_FileSelect.setMinimumSize(QtCore.QSize(560, 0))
        self.lineEdit_FileSelect.setObjectName("lineEdit_FileSelect")
        self.horizontalLayout_FileSelect.addWidget(self.lineEdit_FileSelect)
        self.pushButton_FileSelect = QtWidgets.QPushButton(self.groupBox_FileSelect)
        self.pushButton_FileSelect.setMaximumSize(QtCore.QSize(30, 16777215))
        self.pushButton_FileSelect.setObjectName("pushButton_FileSelect")
        self.horizontalLayout_FileSelect.addWidget(self.pushButton_FileSelect)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_FileSelect.addItem(spacerItem)
        self.pushButton_FileSelect_Convert = QtWidgets.QPushButton(self.groupBox_FileSelect)
        self.pushButton_FileSelect_Convert.setObjectName("pushButton_FileSelect_Convert")
        self.horizontalLayout_FileSelect.addWidget(self.pushButton_FileSelect_Convert)
        self.verticalLayout_4.addLayout(self.horizontalLayout_FileSelect)
        self.verticalLayout.addWidget(self.groupBox_FileSelect)
        self.groupBox_FolderSelect = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_FolderSelect.setMaximumSize(QtCore.QSize(16777215, 80))
        self.groupBox_FolderSelect.setObjectName("groupBox_FolderSelect")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(self.groupBox_FolderSelect)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_FolderSelect = QtWidgets.QHBoxLayout()
        self.horizontalLayout_FolderSelect.setObjectName("horizontalLayout_FolderSelect")
        self.lineEdit_FolderSelect = QtWidgets.QLineEdit(self.groupBox_FolderSelect)
        self.lineEdit_FolderSelect.setMinimumSize(QtCore.QSize(560, 0))
        self.lineEdit_FolderSelect.setObjectName("lineEdit_FolderSelect")
        self.horizontalLayout_FolderSelect.addWidget(self.lineEdit_FolderSelect)
        self.pushButton_FolderSelect = QtWidgets.QPushButton(self.groupBox_FolderSelect)
        self.pushButton_FolderSelect.setMaximumSize(QtCore.QSize(30, 16777215))
        self.pushButton_FolderSelect.setObjectName("pushButton_FolderSelect")
        self.horizontalLayout_FolderSelect.addWidget(self.pushButton_FolderSelect)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_FolderSelect.addItem(spacerItem1)
        self.pushButton_FolderSelect_Convert = QtWidgets.QPushButton(self.groupBox_FolderSelect)
        self.pushButton_FolderSelect_Convert.setObjectName("pushButton_FolderSelect_Convert")
        self.horizontalLayout_FolderSelect.addWidget(self.pushButton_FolderSelect_Convert)
        self.verticalLayout_3.addLayout(self.horizontalLayout_FolderSelect)
        self.verticalLayout.addWidget(self.groupBox_FolderSelect)
        self.verticalLayout_TextEdit = QtWidgets.QVBoxLayout()
        self.verticalLayout_TextEdit.setObjectName("verticalLayout_TextEdit")
        self.textEdit = Qsci.QsciScintilla(self.centralwidget)
        font = QtGui.QFont()
        font.setFamily("Courier New")
        self.textEdit.setFont(font)
        self.textEdit.setToolTip("")
        self.textEdit.setWhatsThis("")
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout_TextEdit.addWidget(self.textEdit)
        self.verticalLayout.addLayout(self.verticalLayout_TextEdit)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "toJSON Converter"))
        self.label_Title.setText(_translate("MainWindow", "<h2>This tool converts JavaScript arrays to JSON data...</h2>"))
        self.groupBox_FileSelect.setTitle(_translate("MainWindow", "Select a file:"))
        self.pushButton_FileSelect.setText(_translate("MainWindow", "..."))
        self.pushButton_FileSelect_Convert.setText(_translate("MainWindow", "Convert"))
        self.groupBox_FolderSelect.setTitle(_translate("MainWindow", "Select a folder with files:"))
        self.pushButton_FolderSelect.setText(_translate("MainWindow", "..."))
        self.pushButton_FolderSelect_Convert.setText(_translate("MainWindow", "Convert"))

from PyQt5 import Qsci
