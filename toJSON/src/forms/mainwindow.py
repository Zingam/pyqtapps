__author__ = 'Hristo'

# Python 3.4 import
import os

# PyQt5 imports
from PyQt5.QtCore import (pyqtSlot)
from PyQt5.QtWidgets import (QMainWindow, QFileDialog, QMessageBox)

# Custom imports
from forms.ui_mainwindow import Ui_MainWindow
from ui.codeViewer import CodeViewer
import logic.ConverterToJson as logic


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        # Initialize variables
        self.pathToOutput = "output"

        # Setup the UI
        self.setupUi(self)
        self.codeViewer = CodeViewer(self.textEdit)

# Connect slots
    @pyqtSlot()
    def on_pushButton_FileSelect_clicked(self):
        # Create file dialog
        fileDialog = QFileDialog()
        fileDialog.setWindowTitle("Select file")
        fileDialog.setFileMode(QFileDialog.ExistingFile)
        fileDialog.setNameFilter("*.js")
        fileDialog.fileSelected.connect(self.on_fileDialog_fileSelected)
        # Open file dialog
        fileDialog.exec()

    @pyqtSlot()
    def on_pushButton_FileSelect_Convert_clicked(self):
        print("Converting to JSON")
        pathToFile = self.lineEdit_FileSelect.text()
        logic.convertJsToJson(pathToFile, self.pathToOutput)

    @pyqtSlot()
    def on_pushButton_FolderSelect_clicked(self):
        # Create file dialog
        fileDialog = QFileDialog()
        fileDialog.setWindowTitle("Select folder")
        fileDialog.setFileMode(QFileDialog.Directory)
        fileDialog.fileSelected.connect(self.on_fileDialog_fileSelected)
        # Open file dialog
        fileDialog.exec()

    @pyqtSlot()
    def on_pushButton_FolderSelect_Convert_clicked(self):
        print("Converting to JSON")

    @pyqtSlot(str)
    def on_fileDialog_fileSelected(self, fileName):
        # Check if file or folder was selected
        if os.path.isfile(fileName):
            self.lineEdit_FileSelect.setText(fileName)
        elif os.path.isdir(fileName):
            self.lineEdit_FolderSelect.setText(fileName)
        else:
            QMessageBox.critical(None,
                                 "ERROR",
                                 "Neither file nor folder was selected!")



