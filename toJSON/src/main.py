__author__ = 'Hristo'

import sys
from PyQt5.QtWidgets import QApplication

from forms.mainwindow import MainWindow

if __name__ == "__main__":
    application = QApplication(sys.argv)

    window = MainWindow()
    window.show()

    application.exec_()