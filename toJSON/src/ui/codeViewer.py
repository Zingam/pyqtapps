__author__ = 'Hristo'

from PyQt5.QtCore import (QObject, pyqtSlot)

from PyQt5.QtGui import (QColor, QFont, QFontMetrics)
from PyQt5.Qsci import (QsciScintilla, QsciLexerJavaScript)


class CodeViewer(QObject):
    """QScintilla based viewer for JSON"""
    def __init__(self, qsciScintillaWidget,  parent=None):
        super().__init__(parent)

        self.qsciScintillaWidget = qsciScintillaWidget

        self.qsciScintillaWidget.setReadOnly(True)

        # Set default font
        font = QFont()
        font.setFamily("Courier New")
        font.setFixedPitch(True)
        font.setPointSize(14)

        self.qsciScintillaWidget.setFont(font)

        # Set margin 0 - used for line numbers
        self.fontMetrics = QFontMetrics(font)
        self.qsciScintillaWidget.setMarginsFont(font)
        self.qsciScintillaWidget.setMarginLineNumbers(0, True)
        self.qsciScintillaWidget.setMarginsBackgroundColor(QColor("#444444"))
        self.qsciScintillaWidget.setMarginsForegroundColor(QColor("#A9B7C6"))
        self.qsciScintillaWidget.setMarginWidth(1, 0)

        self.qsciScintillaWidget.setBraceMatching(QsciScintilla.SloppyBraceMatch)

        lexer = QsciLexerJavaScript()

        lexer.setFont(font)
        # Background color
        lexer.setPaper(QColor("#333333"))
        lexer.setDefaultPaper(QColor("#333333"))
        # Text color
        lexer.setDefaultColor(QColor("#00CCCC"))
        # Text colorization
        # Colorization for: 'some string'
        lexer.setColor(QColor("#A5C261"), QsciLexerJavaScript.SingleQuotedString)
        # Colorization for: "some string"
        lexer.setColor(QColor("#A5C261"), QsciLexerJavaScript.DoubleQuotedString)
        # Colorization for: 13 33 45
        lexer.setColor(QColor("#6897BB"), QsciLexerJavaScript.Number)
        # Colorization for: {} + = ; :
        lexer.setColor(QColor("#A9B7C6"), QsciLexerJavaScript.Operator)
        # Colorization for: true, false
        lexer.setColor(QColor("#FF00BB"), QsciLexerJavaScript.Keyword)
        # Colorization for: valueOfSomething, functionAdd()
        lexer.setColor(QColor("#FF00BB"), QsciLexerJavaScript.Identifier)

        self.qsciScintillaWidget.setLexer(lexer)

        self.qsciScintillaWidget.textChanged.connect(self.on_qsciScintillaWidget_textChanged)

    @pyqtSlot()
    def on_qsciScintillaWidget_textChanged(self):
        marginWidth = self.fontMetrics.width(str(self.qsciScintillaWidget.lines())) + 10
        self.qsciScintillaWidget.setMarginWidth(0, marginWidth)
        self.qsciScintillaWidget.setMarginWidth(1, 5)