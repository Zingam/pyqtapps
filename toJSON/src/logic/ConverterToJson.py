__author__ = 'Hristo'

import io
import re


def convertJsToJson(pathToInputFile, pathToOutputFile):
    print("Converting ", pathToInputFile, " to ", pathToOutputFile)

    # Open file in read-only mode and iterate through all lines
    with io.open(pathToInputFile, "r") as currentFile:
        for currentLine in currentFile:
            newLine = re.findall(r'var.*?=.*?\(\s*(.*?)\);', currentLine)
            if not newLine:
                continue
            else:
                newLine = re.findall(r'var.*?=.*?new.*?Array.*\(\s*(.*?)\);', currentLine)

            # Print current line without new-line ending
            print(newLine, flush=True)