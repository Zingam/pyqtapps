#!/usr/bin/env python
__author__ = 'Hristo'

import os
import shutil

print("Cleaning project structure...")

pathName = "."
fileToRemove = "__pycache__"
            
for subdirectoryPath, subdirectoryName, fileName in os.walk(pathName):
        if fileToRemove in subdirectoryPath:
            print("Removing: " + subdirectoryPath)
            shutil.rmtree(subdirectoryPath)