__author__ = 'Hristo'

# Python imports
import os
import subprocess
import shutil

# PyQt5 imports
from PyQt5.QtCore import (pyqtSlot,
                          QSettings,
                          Qt)
from PyQt5.QtWidgets import (QAbstractButton,
                             QDialogButtonBox,
                             QDialog,
                             QFileDialog,
                             QMessageBox)

# Custom imports
import utilities.utilities as utilities
from forms.ui_maindialog import Ui_Dialog

###################################################################################################
# Constants
###################################################################################################
keyPathToApplicationBinary = "pathToApplicationBinary"
keyPathToQmlFiles = "pathToQmlFiles"
keyPathToQtSdk = "pathToQtSdk"

redistributableFilesList_MinGw = ["libwinpthread-1.dll",
                                  "libgcc_s_dw2-1.dll",
                                  "libstdc++-6.dll"]
redistributableFilesList_VisualStudio = []

###################################################################################################


class MainDialog(QDialog, Ui_Dialog):
    def __init__(self):
        QDialog.__init__(self)

        self.settings = QSettings()

        # Remove ? from TitleBar
        currentWindowFlags = self.windowFlags()
        currentWindowFlags &= not Qt.WindowContextHelpButtonHint
        self.setWindowFlags(currentWindowFlags)

        # Setup the UI
        self.setupUi(self)
        # Disable this for now
        self.radioButton_VisualStudio.setEnabled(False)

        pathToApplicationBinary = self.settings.value(keyPathToApplicationBinary)
        pathToQmlFiles = self.settings.value(keyPathToQmlFiles)
        pathToQtSdk = self.settings.value(keyPathToQtSdk)

        self.lineEdit_PathToApplicationBinary.setText(pathToApplicationBinary)
        self.lineEdit_PathToQmlFiles.setText(pathToQmlFiles)
        self.lineEdit_PathToQtSdk.setText(pathToQtSdk)

        self.isCommandExecutionSuccessful = False;

    # METHODS:
    def copyAdditionalFiles(self):
        """
        Copies additional non-Qt redistributable files if user has checked:
        self.checkBox_CopyAdditionalRedistributableFiles
        :return: nothing
        """
        if (not self.checkBox_CopyAdditionalRedistributableFiles.isChecked
            or not self.isCommandExecutionSuccessful):
            return

        self.textEdit_CommandLineOutput.append(">>> Updating non-Qt redistributable:")

        # Python has interesting variable scope (look below):
        # redistributableFilesList will still be accessible outside of this if-else statement
        if self.radioButton_MinGw.isChecked():
            redistributableFilesList = redistributableFilesList_MinGw
        else:
            # A default declaration is necessary or a trouble could happen if the variable
            # is referenced before declared
            redistributableFilesList = []

        # RedistributableFilesList is accessible although defined in if-else statement
        for file in redistributableFilesList:
            pathCurrentFile = self.lineEdit_PathToQtSdk.text() + "/" + file
            # Normalize path to current operating system format
            pathCurrentFile = os.path.normpath(pathCurrentFile)
            pathToApplicationBinary = self.lineEdit_PathToApplicationBinary.text()
            pathToApplicationBinary = os.path.dirname(pathToApplicationBinary)

            try:
                shutil.copy(pathCurrentFile, pathToApplicationBinary)

            except EnvironmentError:
                errorMessage = "Unable to copy file:" \
                               + pathCurrentFile \
                               + "\nto " \
                               + pathToApplicationBinary
                self.textEdit_CommandLineOutput.append(errorMessage)

            else:
                message = "Copying: " \
                          + pathCurrentFile
                self.textEdit_CommandLineOutput.append(message)

    def execute_windeployqt(self):
        """
        Calls windeployqt.exe as subprocess and displays the output in
        self.textEdit_CommandLineOutput
        :return: nothing
        """
        pathToQtSdk = self.lineEdit_PathToQtSdk.text()
        pathToApplicationBinary = self.lineEdit_PathToApplicationBinary.text()
        pathToQmlFiles = self.lineEdit_PathToQmlFiles.text()

        try:
            if not pathToQtSdk:
                raise Exception("No path to QtSDK was selected!")
            if not pathToApplicationBinary:
                raise Exception("No path to application binary was selected!")

        except Exception as errorMessage:
            QMessageBox.critical(None,
                                 "ERROR!!!",
                                 str(errorMessage)
                                 + "\n"
                                 + "Please select a correct path!")

            return

        qmlOptions = ""

        if pathToQmlFiles:
            qmlOptions = "--qmldir" + pathToQmlFiles

        pathEnvironmentVariable = os.environ["PATH"]
        pathEnvironmentVariable += ";" + pathToQtSdk + ";"
        os.environ["PATH"] = pathEnvironmentVariable

        command = "windeployqt.exe" + " " + qmlOptions + " " + pathToApplicationBinary

        self.textEdit_CommandLineOutput.append("Executing: " + command)
        print("Executing: ", command)

        self.buttonBox.setEnabled(False)

        # Call a system process
        try:
            # universal_newlines - makes manual decoding of subprocess.stdout unnecessary
            output = subprocess.check_output(command,
                                             stderr=subprocess.STDOUT,
                                             universal_newlines=True)

            # Print out command's standard output (elegant)
            for currentLine in output:
                self.textEdit_CommandLineOutput.insertPlainText(currentLine)

            self.isCommandExecutionSuccessful = True

        except subprocess.CalledProcessError as error:
            self.isCommandExecutionSuccessful = False

            errorMessage = ">>> Error while executing:\n"\
                           + command\
                           + "\n>>> Returned with error:\n"\
                           + str(error.output)
            self.textEdit_CommandLineOutput.append(errorMessage)

            QMessageBox.critical(None,
                                 "ERROR",
                                 errorMessage)
            print("Error: " + errorMessage)

        except FileNotFoundError as error:
            errorMessage = error.strerror
            QMessageBox.critical(None,
                                 "ERROR",
                                 errorMessage)
            print("Error: ", errorMessage)

        self.copyAdditionalFiles()

        self.buttonBox.setEnabled(True)

    @staticmethod
    def getNormalizedPath(fileDialog):
        """
        Returns normalized path to the first folder in the list of selected folders
        :param fileDialog:
        :return: path to selected file/folder
        """
        # Normalize a pathname by collapsing redundant separators and up-level references.
        # On Windows, it converts forward slashes to backward slashes.
        foldersList = fileDialog.selectedFiles()

        return os.path.normpath(foldersList[0])

    # SLOTS:
    # To connect this slot a parameter must be passed
    # In this case "button" is of type "QAbstractButton"
    @pyqtSlot(QAbstractButton)
    def on_buttonBox_clicked(self, button):
        # This gets the type of the current button
        currentButton = self.buttonBox.standardButton(button)
        if QDialogButtonBox.Apply == currentButton:
            self.execute_windeployqt()
        elif QDialogButtonBox.Close == currentButton:
            print("Closing: Good bye!")
        else:
            print("Unsupported button clicked in ",
                  __file__,
                  " at  line ",
                  utilities.getCurrentLineNumber())

    @pyqtSlot(int)
    def on_checkBox_CopyAdditionalRedistributableFiles_stateChanged(self, state):
        if Qt.Checked == state:
            self.groupBox_CopyAdditionalRedistributableFiles.setEnabled(True)
        elif Qt.Unchecked == state:
            self.groupBox_CopyAdditionalRedistributableFiles.setEnabled(False)
        else:
            print("Invalid state checked!")

    @pyqtSlot()
    def on_pushButton_Clear_clicked(self):
        self.textEdit_CommandLineOutput.clear()

    @pyqtSlot()
    def on_pushButton_PathToApplicationBinary_Select_clicked(self):
        # Create file dialog
        fileDialog = QFileDialog()
        fileDialog.setWindowTitle("Select a Qt application binary...")
        fileDialog.setFileMode(QFileDialog.ExistingFile)
        fileDialog.setNameFilter("*.exe")

        # Open file dialog
        wasFileSelected = fileDialog.exec()
        if wasFileSelected:
            selectedFile = self.getNormalizedPath(fileDialog)
            self.lineEdit_PathToApplicationBinary.setText(selectedFile)
            self.settings.setValue(keyPathToApplicationBinary, selectedFile)

    @pyqtSlot()
    def on_pushButton_PathToQmlFiles_Select_clicked(self):
        # Create file dialog
        fileDialog = QFileDialog()
        fileDialog.setWindowTitle("Select path to Qml files...")
        fileDialog.setFileMode(QFileDialog.Directory)

        # Open file dialog
        wasFileSelected = fileDialog.exec()
        if wasFileSelected:
            selectedFolder = self.getNormalizedPath(fileDialog)
            self.lineEdit_PathToQmlFiles.setText(selectedFolder)
            self.settings.setValue(keyPathToQmlFiles, selectedFolder)

    @pyqtSlot()
    def on_pushButton_PathToQtSdk_Select_clicked(self):
        # Create file dialog
        fileDialog = QFileDialog()
        fileDialog.setWindowTitle("Select path to Qt SDK...")
        fileDialog.setFileMode(QFileDialog.Directory)

        # Open file dialog
        wasFileSelected = fileDialog.exec()
        if wasFileSelected:
            selectedFolder = self.getNormalizedPath(fileDialog)
            self.lineEdit_PathToQtSdk.setText(selectedFolder)
            self.settings.setValue(keyPathToQtSdk, selectedFolder)