__author__ = 'Hristo'


import sys
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtWidgets import QApplication

from forms.maindialog import MainDialog


if __name__ == "__main__":
    QCoreApplication.setOrganizationName("Roccoor Multimedia");
    QCoreApplication.setOrganizationDomain("roccoor.com");
    QCoreApplication.setApplicationName("QtDeploy")

    application = QApplication(sys.argv)

    dialog = MainDialog()
    dialog.open()

    application.exec_()