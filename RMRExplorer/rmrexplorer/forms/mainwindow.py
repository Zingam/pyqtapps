# Python 3.4 imports

# PyQt5 imports
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets

# Custom imports
from rmrexplorer.forms.dialogabout import DialogAbout
from rmrexplorer.forms.dialogbuildresourcefile import DialogBuildResourceFile
from rmrexplorer.forms.ui_mainwindow import Ui_MainWindow
import rmrexplorer.utilities as utilities

# Authorship information
__author__ = 'Hristo'


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        # Call base class' constructor
        QtWidgets.QMainWindow.__init__(self)

        self._trClassName = "MainWindow"
        self._tr = QtCore.QCoreApplication.translate

        # Load and set window icon from resource file
        windowIcon = QtGui.QIcon(":RMRExplorer/icons/resource_file_new")
        self.setWindowIcon(windowIcon)

        # Setup UI
        # import rmrexplorer.resources.resources_rc  # Imports icons resource
        # del rmrexplorer.resources.resources_rc
        self.setupUi(self)

    @QtCore.pyqtSlot()
    def on_actionAbout_triggered(self):
        """Shows about dialog."""
        dialogAbout = DialogAbout()

        dialogAbout.exec()

    @QtCore.pyqtSlot()
    def on_actionExit_triggered(self):
        """Exits the application."""
        QtCore.QCoreApplication.quit()

    @QtCore.pyqtSlot()
    def on_actionFileAdd_triggered(self):
        # TODO: This feature is not yet implemented
        utilities.displayInimplementedWarning(self, "on_actionFileAdd_triggered")
        return

        fileDialog = QtWidgets.QFileDialog()
        fileDialog.setWindowTitle(self._tr(self._trClassName, "Select File"))

        fileDialog.exec()

    @QtCore.pyqtSlot()
    def on_actionFileDelete_triggered(self):
        # TODO: This feature is not yet implemented
        utilities.displayInimplementedWarning(self, "on_actionFileDelete_triggered")
        return

    @QtCore.pyqtSlot()
    def on_actionResourceFileCreate_triggered(self):
        # TODO: This feature is not yet implemented
        utilities.displayInimplementedWarning(self, "on_actionResourceCreate_triggered")
        return

    @QtCore.pyqtSlot()
    def on_actionResourceFileBuild_triggered(self):
        fileDialog = DialogBuildResourceFile()

        fileDialog.exec()

    @QtCore.pyqtSlot()
    def on_actionResourceFileOpen_triggered(self):
        # TODO: This feature is not yet implemented
        utilities.displayInimplementedWarning(self, "on_actionResourceFileOpen_triggered")
        return

        fileDialog = QtWidgets.QFileDialog()
        fileDialog.setNameFilter(self._tr(self._trClassName, "RMResourceFile file") + " (*.rmr)")
        fileDialog.setWindowTitle(self._tr(self._trClassName, "Open RMResourceFile File"))

        fileDialog.exec()

    @QtCore.pyqtSlot()
    def on_actionResourceFileSave_triggered(self):
        # TODO: This feature is not yet implemented
        utilities.displayInimplementedWarning(self, "on_actionResourceFileSave_triggered")
        return

        fileDialog = QtWidgets.QFileDialog()
        fileDialog.setAcceptMode(QtWidgets.QFileDialog.AcceptSave)
        fileDialog.setNameFilter(self._tr(self._trClassName, "RMResource file") + " (*.rmr)")
        fileDialog.setWindowTitle(self._tr(self._trClassName, "Save RMResource File As"))

        fileDialog.exec()



