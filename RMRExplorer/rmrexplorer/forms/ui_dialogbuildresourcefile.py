# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Mira\Documents\PyQtApps\RMRExplorer\rmrexplorer\forms\dialogbuildresourcefile.ui'
#
# Created: Fri Oct 10 10:46:44 2014
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogBuildResourceFile(object):
    def setupUi(self, DialogBuildResourceFile):
        DialogBuildResourceFile.setObjectName("DialogBuildResourceFile")
        DialogBuildResourceFile.resize(611, 422)
        self.verticalLayoutDialogBuildResource = QtWidgets.QVBoxLayout(DialogBuildResourceFile)
        self.verticalLayoutDialogBuildResource.setObjectName("verticalLayoutDialogBuildResource")
        self.groupBoxSourceFolder = QtWidgets.QGroupBox(DialogBuildResourceFile)
        self.groupBoxSourceFolder.setObjectName("groupBoxSourceFolder")
        self.horizontalLayoutSourceFolder = QtWidgets.QHBoxLayout(self.groupBoxSourceFolder)
        self.horizontalLayoutSourceFolder.setObjectName("horizontalLayoutSourceFolder")
        self.lineEditSourceFolderFullPath = QtWidgets.QLineEdit(self.groupBoxSourceFolder)
        self.lineEditSourceFolderFullPath.setObjectName("lineEditSourceFolderFullPath")
        self.horizontalLayoutSourceFolder.addWidget(self.lineEditSourceFolderFullPath)
        self.pushButtonSourceFolderFullPathSelect = QtWidgets.QPushButton(self.groupBoxSourceFolder)
        self.pushButtonSourceFolderFullPathSelect.setObjectName("pushButtonSourceFolderFullPathSelect")
        self.buttonGroup_ButtonsSelect = QtWidgets.QButtonGroup(DialogBuildResourceFile)
        self.buttonGroup_ButtonsSelect.setObjectName("buttonGroup_ButtonsSelect")
        self.buttonGroup_ButtonsSelect.addButton(self.pushButtonSourceFolderFullPathSelect)
        self.horizontalLayoutSourceFolder.addWidget(self.pushButtonSourceFolderFullPathSelect)
        self.verticalLayoutDialogBuildResource.addWidget(self.groupBoxSourceFolder)
        self.groupBoxResourceFile = QtWidgets.QGroupBox(DialogBuildResourceFile)
        self.groupBoxResourceFile.setObjectName("groupBoxResourceFile")
        self.horizontalLayoutGroupBoxResourceFile = QtWidgets.QHBoxLayout(self.groupBoxResourceFile)
        self.horizontalLayoutGroupBoxResourceFile.setObjectName("horizontalLayoutGroupBoxResourceFile")
        self.lineEditResourceFileFullPath = QtWidgets.QLineEdit(self.groupBoxResourceFile)
        self.lineEditResourceFileFullPath.setObjectName("lineEditResourceFileFullPath")
        self.horizontalLayoutGroupBoxResourceFile.addWidget(self.lineEditResourceFileFullPath)
        self.pushButtonResourceFileFullPathSelect = QtWidgets.QPushButton(self.groupBoxResourceFile)
        self.pushButtonResourceFileFullPathSelect.setObjectName("pushButtonResourceFileFullPathSelect")
        self.buttonGroup_ButtonsSelect.addButton(self.pushButtonResourceFileFullPathSelect)
        self.horizontalLayoutGroupBoxResourceFile.addWidget(self.pushButtonResourceFileFullPathSelect)
        self.verticalLayoutDialogBuildResource.addWidget(self.groupBoxResourceFile)
        self.horizontalLayoutCurrentProgress = QtWidgets.QHBoxLayout()
        self.horizontalLayoutCurrentProgress.setObjectName("horizontalLayoutCurrentProgress")
        self.labelCurrentProgressIcon = QtWidgets.QLabel(DialogBuildResourceFile)
        self.labelCurrentProgressIcon.setMaximumSize(QtCore.QSize(25, 25))
        self.labelCurrentProgressIcon.setText("")
        self.labelCurrentProgressIcon.setScaledContents(True)
        self.labelCurrentProgressIcon.setObjectName("labelCurrentProgressIcon")
        self.horizontalLayoutCurrentProgress.addWidget(self.labelCurrentProgressIcon)
        self.labelCurrentProgress = QtWidgets.QLabel(DialogBuildResourceFile)
        self.labelCurrentProgress.setText("")
        self.labelCurrentProgress.setObjectName("labelCurrentProgress")
        self.horizontalLayoutCurrentProgress.addWidget(self.labelCurrentProgress)
        self.verticalLayoutDialogBuildResource.addLayout(self.horizontalLayoutCurrentProgress)
        self.treeViewResourceFile = QtWidgets.QTreeView(DialogBuildResourceFile)
        self.treeViewResourceFile.setObjectName("treeViewResourceFile")
        self.verticalLayoutDialogBuildResource.addWidget(self.treeViewResourceFile)
        self.horizontalLayoutDialogButtons = QtWidgets.QHBoxLayout()
        self.horizontalLayoutDialogButtons.setObjectName("horizontalLayoutDialogButtons")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayoutDialogButtons.addItem(spacerItem)
        self.pushButtonClose = QtWidgets.QPushButton(DialogBuildResourceFile)
        self.pushButtonClose.setObjectName("pushButtonClose")
        self.buttonGroupDialogButtonsDialog = QtWidgets.QButtonGroup(DialogBuildResourceFile)
        self.buttonGroupDialogButtonsDialog.setObjectName("buttonGroupDialogButtonsDialog")
        self.buttonGroupDialogButtonsDialog.addButton(self.pushButtonClose)
        self.horizontalLayoutDialogButtons.addWidget(self.pushButtonClose)
        self.pushButtonBuild = QtWidgets.QPushButton(DialogBuildResourceFile)
        self.pushButtonBuild.setObjectName("pushButtonBuild")
        self.buttonGroupDialogButtonsDialog.addButton(self.pushButtonBuild)
        self.horizontalLayoutDialogButtons.addWidget(self.pushButtonBuild)
        self.verticalLayoutDialogBuildResource.addLayout(self.horizontalLayoutDialogButtons)

        self.retranslateUi(DialogBuildResourceFile)
        QtCore.QMetaObject.connectSlotsByName(DialogBuildResourceFile)

    def retranslateUi(self, DialogBuildResourceFile):
        _translate = QtCore.QCoreApplication.translate
        DialogBuildResourceFile.setWindowTitle(_translate("DialogBuildResourceFile", "Build resource file"))
        self.groupBoxSourceFolder.setTitle(_translate("DialogBuildResourceFile", "Source folder"))
        self.pushButtonSourceFolderFullPathSelect.setText(_translate("DialogBuildResourceFile", "..."))
        self.groupBoxResourceFile.setTitle(_translate("DialogBuildResourceFile", "Resource file name"))
        self.pushButtonResourceFileFullPathSelect.setText(_translate("DialogBuildResourceFile", "..."))
        self.pushButtonClose.setText(_translate("DialogBuildResourceFile", "Close"))
        self.pushButtonBuild.setText(_translate("DialogBuildResourceFile", "Build"))

