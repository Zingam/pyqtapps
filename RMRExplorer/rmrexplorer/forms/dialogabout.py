# Python 3.4 imports

# PyQt5 imports
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets

# Custom imports
from rmrexplorer.forms.ui_dialogabout import Ui_DialogAbout

# Authorship information
__author__ = 'Hristo'


class DialogAbout(QtWidgets.QDialog, Ui_DialogAbout):
    def __init__(self):
        # Call base class' constructor
        QtWidgets.QDialog.__init__(self)

        # Load and set window icon from resource file
        windowIcon = QtGui.QIcon(":RMRExplorer/icons/resource_file_new")
        self.setWindowIcon(windowIcon)

        # Remove context help button ? from the dialog
        windowsFlags = self.windowFlags()
        windowsFlags &= ~QtCore.Qt.WindowContextHelpButtonHint
        self.setWindowFlags(windowsFlags)

        # Setup UI
        self.setupUi(self)