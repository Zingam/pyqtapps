# Python 3.4 imports

# PyQt5 imports
import PyQt5.QtCore as QtCore
import PyQt5.QtGui as QtGui
import PyQt5.QtWidgets as QtWidgets

# Custom imports
from rmrexplorer.forms.ui_dialogbuildresourcefile import Ui_DialogBuildResourceFile

# Authorship information
__author__ = 'Hristo'


class DialogBuildResourceFile(QtWidgets.QDialog, Ui_DialogBuildResourceFile):
    def __init__(self):
        # Call base class' constructor
        QtWidgets.QDialog.__init__(self)

        # Load and set window icon from resource file
        windowIcon = QtGui.QIcon(":RMRExplorer/icons/resource_file_build")
        self.setWindowIcon(windowIcon)

        # Remove context help button ? from the dialog
        windowsFlags = self.windowFlags()
        windowsFlags &= ~QtCore.Qt.WindowContextHelpButtonHint
        self.setWindowFlags(windowsFlags)

        # Setup UI
        self.setupUi(self)

    @QtCore.pyqtSlot()
    def on_pushButtonBuild_clicked(self):
        self.labelCurrentProgress.setText("Building resource file...")
        pixmap = QtGui.QPixmap(":RMRExplorer/icons/resource_file_build")
        self.labelCurrentProgressIcon.setPixmap(pixmap)

    @QtCore.pyqtSlot()
    def on_pushButtonClose_clicked(self):
        self.close()