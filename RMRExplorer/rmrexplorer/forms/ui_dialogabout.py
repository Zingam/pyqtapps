# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\Mira\Documents\PyQtApps\RMRExplorer\rmrexplorer\forms\dialogabout.ui'
#
# Created: Fri Oct 10 10:46:44 2014
#      by: PyQt5 UI code generator 5.3.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogAbout(object):
    def setupUi(self, DialogAbout):
        DialogAbout.setObjectName("DialogAbout")
        DialogAbout.resize(500, 300)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(DialogAbout.sizePolicy().hasHeightForWidth())
        DialogAbout.setSizePolicy(sizePolicy)
        DialogAbout.setMinimumSize(QtCore.QSize(500, 300))
        DialogAbout.setMaximumSize(QtCore.QSize(500, 300))
        self.verticalLayoutDialogAbout = QtWidgets.QVBoxLayout(DialogAbout)
        self.verticalLayoutDialogAbout.setObjectName("verticalLayoutDialogAbout")
        self.labelApplicationName = QtWidgets.QLabel(DialogAbout)
        font = QtGui.QFont()
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        self.labelApplicationName.setFont(font)
        self.labelApplicationName.setAlignment(QtCore.Qt.AlignCenter)
        self.labelApplicationName.setObjectName("labelApplicationName")
        self.verticalLayoutDialogAbout.addWidget(self.labelApplicationName)
        self.groupBoxLogo = QtWidgets.QGroupBox(DialogAbout)
        self.groupBoxLogo.setObjectName("groupBoxLogo")
        self.verticalLayoutGroupBoxLogo = QtWidgets.QVBoxLayout(self.groupBoxLogo)
        self.verticalLayoutGroupBoxLogo.setObjectName("verticalLayoutGroupBoxLogo")
        self.labelLogo = QtWidgets.QLabel(self.groupBoxLogo)
        font = QtGui.QFont()
        font.setPointSize(16)
        self.labelLogo.setFont(font)
        self.labelLogo.setAlignment(QtCore.Qt.AlignCenter)
        self.labelLogo.setObjectName("labelLogo")
        self.verticalLayoutGroupBoxLogo.addWidget(self.labelLogo)
        self.labelApplicationVersion = QtWidgets.QLabel(self.groupBoxLogo)
        self.labelApplicationVersion.setAlignment(QtCore.Qt.AlignCenter)
        self.labelApplicationVersion.setObjectName("labelApplicationVersion")
        self.verticalLayoutGroupBoxLogo.addWidget(self.labelApplicationVersion)
        self.verticalLayoutDialogAbout.addWidget(self.groupBoxLogo)
        self.verticalLayoutCredits = QtWidgets.QVBoxLayout()
        self.verticalLayoutCredits.setObjectName("verticalLayoutCredits")
        self.labelCretids = QtWidgets.QLabel(DialogAbout)
        self.labelCretids.setOpenExternalLinks(True)
        self.labelCretids.setObjectName("labelCretids")
        self.verticalLayoutCredits.addWidget(self.labelCretids)
        self.verticalLayoutDialogAbout.addLayout(self.verticalLayoutCredits)
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogAbout)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayoutDialogAbout.addWidget(self.buttonBox)

        self.retranslateUi(DialogAbout)
        self.buttonBox.accepted.connect(DialogAbout.accept)
        self.buttonBox.rejected.connect(DialogAbout.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogAbout)

    def retranslateUi(self, DialogAbout):
        _translate = QtCore.QCoreApplication.translate
        DialogAbout.setWindowTitle(_translate("DialogAbout", "About"))
        self.labelApplicationName.setText(_translate("DialogAbout", "RMR Explorer"))
        self.groupBoxLogo.setTitle(_translate("DialogAbout", "Made by"))
        self.labelLogo.setText(_translate("DialogAbout", "Roccoor Multimedia"))
        self.labelApplicationVersion.setText(_translate("DialogAbout", "Version 0.0.1.0"))
        self.labelCretids.setText(_translate("DialogAbout", "Icons by <a href=\"http://icons8.com/\">Icons8</a>"))

