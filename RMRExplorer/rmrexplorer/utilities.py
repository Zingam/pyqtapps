# Python 3.4 imports

# PyQt5 imports
import PyQt5.QtWidgets as QtWidgets

# Custom imports

# Authorship information
__author__ = 'Hristo'


def displayInimplementedWarning(widget: QtWidgets.QWidget, featureName: str):
    QtWidgets.QMessageBox.critical(widget,
                                   "Feature is not implemented!",
                                   "This feature is not yet implemented: " + featureName,
                                   QtWidgets.QMessageBox.Ok)
