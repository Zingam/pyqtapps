#!/usr/bin/env python
""" RMRExplorer
"""

# Python imports
import sys

# PyQt imports
import PyQt5.QtWidgets as QtWidgets

# Custom imports
from rmrexplorer.forms.mainwindow import MainWindow

# Authorship information
__author__ = 'Hristo'


def main():
    """This application's main function"""
    application = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()

    application.exec()


###############################################################################
# Main entry point
###############################################################################
if "__main__" == __name__:
    """ __name__ is an attribute of a module. If the current module is executed by the Python
        interpreter as the main program "__name__" is set to "__main__".
        Thus the following code will be executed only if the current module is exectuted as a
        main program.
    """
    main()
###############################################################################