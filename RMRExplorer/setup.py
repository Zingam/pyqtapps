try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

attributes = {
    'name': "RMRExplorer",
    'version': "0.0.1.0",
    'packages': ["rmrexplorer"],
    'url': "",
    'license': "",
    'author': "Hristo",
    'author_email': "",
    'description': "RMResource Manager application"
}

setup(**attributes)
