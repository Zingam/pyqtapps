#!/usr/bin/env python
""" makeforms.py
    Re-Builds all Qt Designer .ui files in all subdirectories of the current directory.
"""

# Python imports
import os
import fnmatch
import sys
import platform
import subprocess

# Authorship information
__author__ = 'Hristo'


def normalize_path(path: str):
    """Normalizes a path to the underlying operating system's conventions.

    On Windows converts all "/" to "\".
    On Linux converts all "\" to "/".

    Args:
        :type path: str
        :param path:
            Path to be normalized.

    Returns:
        :type return: str
        :return:
            Normalized path if the underlying operating system is "Windows"
            or "Linux", otherwise returns the path unaltered.
    """
    if "Windows" == platform.system():
        return path.replace("/", "\\")
    elif "Linux" == platform.system():
        return path.replace("\\", "/")
    else:
        return path


def main():
    """This script's main function."""
    print("Building PyQt forms from QtDesigner .ui files")
    print("WARNING: Does not work if filename contains whitespace characters!")

    # Check for old files and delete them
    mainPackage = "rmrexplorer"
    formsPackage = "forms"
    resourcePackage = "resources"
    resourceName = "resources"

    pathForms = mainPackage + "/" + formsPackage + "/"

    for fileName in os.listdir(pathForms):
        if fnmatch.fnmatch(fileName, "ui_*"):
            print("Deleting old file: " + fileName)
            os.remove(pathForms + fileName)

    # Find PyQt5 system path
    pathPython = sys.path
    pathPyQt5 = ""

    for path in pathPython:
        if path.find("site-packages"):
            pathPyQt5 = path

    pathPyQt5 += "/PyQt5"
    if platform.system() == "Windows":
        pathPyQt5 = pathPyQt5.replace("/", "\\")

    print("PyQt5 location: " + pathPyQt5)

    # Find this script's path
    scriptPath = os.path.dirname(os.path.realpath(__file__)) + "/"
    pathFormsFull = scriptPath + pathForms

    # Find .ui files and build them
    for fileName in os.listdir(pathForms):
        if fnmatch.fnmatch(fileName, "*.ui"):
            # Build and execute the command
            # /pyuic5 builds .py files from QtDesigner .ui files
            command = pathPyQt5 + "/pyuic5" + " "
            command += pathFormsFull + fileName + " "
            newFileName = fileName.rstrip(".ui")
            command += "-o" + " "
            command += pathFormsFull + "ui_" + newFileName + ".py"

            command = normalize_path(command)

            print("Executing: " + command)

            # Call a system process
            try:
                subprocess.call(command, shell=True)
            except subprocess.CalledProcessError as error:
                print("Failed to execute command")
                print(command)
                print("Error: " + error.output)

    # Post process the generated files
    for fileName in os.listdir(pathForms):
        if fnmatch.fnmatch(fileName, "ui_*"):
            print("Post processing: " + fileName)

            fileNamePathFull = pathFormsFull + fileName
            fileNamePathFull = normalize_path(fileNamePathFull)
            print(fileNamePathFull)

            with open(fileNamePathFull, "r+") as file:
                fileData = []

                for line in file:
                    line = line.replace("import " + resourceName + "_rc",
                                        "import " + mainPackage + "." + resourcePackage + "." + resourceName + "_rc")
                    fileData.append(line)

                fileData = "".join(fileData)
                file.seek(0)
                file.truncate()
                file.write(fileData)


###############################################################################
# Main entry point
###############################################################################
if "__main__" == __name__:
    main()
###############################################################################