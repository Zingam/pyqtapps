#!/usr/bin/env python
""" cleanup.p
    Deletes Python cache from all subdirectories of the current directory.
"""

# Python imports
import os
import shutil

# Authorship information
__author__ = 'Hristo'


def main():
    """ Main function"""
    print("Cleaning project structure...")

    pathName = "."
    fileToRemove = "__pycache__"

    for subdirectoryPath, subdirectoryName, fileName in os.walk(pathName):
            if fileToRemove in subdirectoryPath:
                print("Removing: " + subdirectoryPath)
                shutil.rmtree(subdirectoryPath)

# Main entry point
if "__main__" == __name__:
    main()