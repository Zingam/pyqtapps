#!/usr/bin/env python
""" makeresources.py
    Re-Builds Qt resource files .qrc files.
"""

# Python imports
import os
import fnmatch
import sys
import platform
import subprocess

# Authorship information
__author__ = 'Hristo'


def main():
    """This script's main function."""
    print("Building PyQt forms from QtDesigner .ui files")
    print("WARNING: Does not work if filename contains whitespace characters!")

    # Check for old files and delete them
    pathResources = "res"
    mainPackage = "rmrexplorer"
    resourcesOutputPathFull = mainPackage + "/resources"

    for file in os.listdir(resourcesOutputPathFull):
        if fnmatch.fnmatch(file, "*_rc.py"):
            print("Deleting old file: " + file)
            os.remove(resourcesOutputPathFull + "/" + file)

    # if os.path.isfile(file):
    #     print("Deleting old files: " + file)
    #     os.remove(file)

    # Find PyQt5 system path
    pathPython = sys.path
    pathPyQt5 = ""

    for path in pathPython:
        if path.find("site-packages"):
            pathPyQt5 = path

    pathPyQt5 += "/PyQt5"
    if platform.system() == "Windows":
        pathPyQt5 = pathPyQt5.replace("/", "\\")

    print("PyQt5 location: " + pathPyQt5)

    # Find this script's path
    scriptPath = os.path.dirname(os.path.realpath(__file__)) + "/"
    pathResourcesFull = scriptPath + pathResources

    # Find .ui files and build them
    for file in os.listdir(pathResources):
        if fnmatch.fnmatch(file, "*.qrc"):
            # Build and execute the command
            # /pyrcc5 builds .py files from Qt .qrc resource files
            command = pathPyQt5 + "/pyrcc5" + " "
            command += pathResourcesFull + "/" + file + " "
            newFileName = file.rstrip(".qrc")
            command += "-o" + " "
            command += mainPackage + "/resources/" + newFileName + "_rc" + ".py"

            if platform.system() == "Windows":
                command = command.replace("/", "\\")

            print("Executing: " + command)

            # Call a system process
            try:
                subprocess.call(command, shell=True)
            except subprocess.CalledProcessError as error:
                print("Failed to execute command")
                print(command)
                print("Error: " + error.output)


###############################################################################
# Main entry point
###############################################################################
if "__main__" == __name__:
    main()
###############################################################################
