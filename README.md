Experimental Apps with PyQt 5.3
==============

```
    print("Experimental projects developed in Qt 5.3")
```

* QtBrowser - Qt 5.3 QWebView based browser
* QtDeploy - GUI for  windeployqt.exe tool
* RMRExplorer - RMResource file build tool
* toJSON - JavaScript arrays to JSON converter

